import Vue from 'vue';
import VueToast from 'vue-toast-notification';
import 'vue-toast-notification/dist/theme-default.css';

function Toast(obj) {
  const { message, type } = obj;
  Vue.use(VueToast);

  Vue.$toast.open({
    message: message,
    position: 'top-right',
    duration: 2500,
    type: type,
  });
}
export default Toast;
