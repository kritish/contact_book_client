import Vue from 'vue';
import VueRouter from 'vue-router';
import axios from 'axios';
import App from './App.vue';
import routes from './routes';
import setAuth from './utils/setAuth';

Vue.config.productionTip = false;

Vue.use(VueRouter);

const router = new VueRouter({
  mode: 'history',
  routes,
});

// set base url for axios
axios.defaults.baseURL = 'http://localhost:8000';
// set axios headers
setAuth();

new Vue({
  router,
  render: (h) => h(App),
}).$mount('#app');
