import axios from 'axios';

function setAuth(token) {
  if (!token) {
    const tokenLocal = localStorage.getItem('token');
    axios.defaults.headers.common['Authorization'] = tokenLocal;
  } else {
    localStorage.setItem('token', token);
    axios.defaults.headers.common['Authorization'] = token;
  }
}

export default setAuth;
